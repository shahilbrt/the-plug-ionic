﻿app.controller('IndexCtrl', ['$scope', '$rootScope', '$state', 'IndexServices', 'sharedUtils', 'ModalService', '$ionicPlatform', '$ionicSideMenuDelegate', 'ProfileServices', function ($scope, $rootScope, $state, IndexServices, sharedUtils, ModalService, $ionicPlatform, $ionicSideMenuDelegate, ProfileServices) {
	ProfileServices.profileUser().success(function (data) {
		$scope.userdata = data.data;

	}).error(function (data) {

	});
	$scope.hamburger = true;
	$rootScope.chargingMode = true;
	$scope.default = localStorage.getItem("timeline") ? true : false;
	$scope.image = localStorage.getItem("image") ? localStorage.getItem("image") : './img/camera.png';
	$scope.userData = JSON.parse(localStorage.getItem("data"));
	$rootScope.fabButton = true;
	$scope.profilePic = function () {
		navigator.camera.getPicture(onSuccess, onFail, {
			quality: 50,
			allowEdit: 1,
			sourceType: 1,
			correctOrientation: 0,
			saveToPhotoAlbum: 1,
			destinationType: Camera.DestinationType.FILE_URI
		});
		function onSuccess(imageURI) {
			$scope.image = imageURI;
			localStorage.setItem("image", imageURI);
			$scope.$digest();
		}
		function onFail(message) {
		}
	};

	$scope.backgroundPic = function () {
		navigator.camera.getPicture(onSuccess, onFail, {
			quality: 50,
			destinationType: Camera.DestinationType.DATA_URL,
			sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
		});
		function onSuccess(imageURI) {
			$scope.default = true;
			$scope.timeline = imageURI;
			localStorage.setItem("timeline", imageURI);
			$scope.$digest();
		}
		function onFail(message) {
		}
	};

	$scope.logout = function () {
		sharedUtils.showLoading();
		IndexServices.logout().success(function (data) {
			localStorage.clear();
			$state.go("app.login");
			sharedUtils.hideLoading();
		}).error(function (data) {
			localStorage.clear();
			$state.go("app.login");
			sharedUtils.hideLoading();
		});
	};
  $scope.getOrders=function () {

      IndexServices.getorders().success(function (data) {

      	$scope.Orders=data.orders;
      	console.log($scope.Orders);

      }).error(function (data) {
          localStorage.clear();
          $state.go("app.login");
          sharedUtils.hideLoading();
      });


  };

    $scope.complainform=function (data) {

        IndexServices.getcomplaint(data).success(function (data) {

            $scope.complaintData=data.order;
            console.log($scope.complaintData);

        }).error(function (data) {
            localStorage.clear();
            $state.go("app.login");
            sharedUtils.hideLoading();
        });


    };
    $scope.sendComplaint=function (data,id) {
    	console.log(data,id);

    }


	$scope.invite = function () {
		ModalService
			.init('templates/invite.html', $scope)
			.then(function (modal) {
				modal.show();
			});
	};

	$scope.complain = function () {
        $scope.getOrders();
		ModalService
			.init('templates/complain.html', $scope)
			.then(function (modal) {
				modal.show();
				$scope.complainModal = modal;
			});
	};

	$scope.orders = function () {
        $scope.getOrders();
		ModalService
			.init('templates/orders.html', $scope)
			.then(function (modal) {
				modal.show();
			});
	};

	$scope.terms = function () {
		ModalService
			.init('templates/terms.html', $scope)
			.then(function (modal) {
				modal.show();
			});
	};

	$scope.complainMail = function (orderid) {
		console.log(orderid)
		$scope.complainform(orderid);
		ModalService
			.init('templates/complainmail.html', $scope)
			.then(function (modal) {
				modal.show();
				$scope.complainmailModal = modal;
			});
	};

	$scope.profilePage = function () {
		$state.go("home.profile");
	};

	$scope.explore = function () {
		$state.go("home.explore");
	};

	$scope.fav = function () {
		$state.go("home.fav");
	};

	$scope.dashboard = function () {
		$state.go("home.dashboard");
	};
	$scope.uploadproduct = function () {
        ModalService
		.init('templates/uploadproduct.html', $scope)
		.then(function (modal) {
			modal.show();
		});
	};

	$scope.$watch(function () {
		return $ionicSideMenuDelegate.isOpenLeft();
	},
		function (isOpen) {
			if (isOpen) {
				$rootScope.fabButton = false;
			}
			else {
				$rootScope.fabButton = true;
			}
		});
}]);