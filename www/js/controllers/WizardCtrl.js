app.controller('WizardCtrl', ['$scope', '$state', '$ionicSlideBoxDelegate', function ($scope, $state, $ionicSlideBoxDelegate) {
  $scope.slideNumber = 0;
  $scope.startApp = function () {
    $state.go('app.login');
  };
  $scope.next = function () {
    $scope.slideNumber++;
    if ($scope.slideNumber == 4) {
      $scope.startApp();
    }
    else {
      $ionicSlideBoxDelegate.next();
    }
  };
  $scope.previous = function () {
    $ionicSlideBoxDelegate.previous();
  };
  // Called each time the slide changes
  $scope.slideChanged = function (index) {
    $scope.slideNumber = index
    $scope.slideIndex = index;
  };
}]);