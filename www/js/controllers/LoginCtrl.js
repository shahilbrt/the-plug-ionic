﻿app.controller('LoginCtrl', ['$scope', 'LoginServices', '$ionicPopup', '$state', 'sharedUtils', 'DashboardService', function ($scope, LoginServices, $ionicPopup, $state, sharedUtils, DashboardService) {
    $scope.user = {};


    $scope.login = function (formName, cred) {
        if (formName.$valid) {
            sharedUtils.showLoading();
            LoginServices.loginUser(cred.email, cred.password).success(function (data) {
                if (data.status) {
                    localStorage.setItem("token", data.token);
                    localStorage.setItem("refreshtoken", data.refreshtoken);
                    localStorage.setItem("data", JSON.stringify(data));
                    $state.go('home.dashboard', {itemData:data});
                }
                else {
                    var alertPopup = $ionicPopup.alert({
                        template: "<center>Invalid Username & Password</center>",
                    });
                }
                sharedUtils.hideLoading();
                $scope.user = {};
            }).error(function (data) {
                sharedUtils.hideLoading();
                var alertPopup = $ionicPopup.alert({
                    template: "<center>Try Again Later</center>"
                });
            });

        }
        else {
        }
    };

    $scope.forgotPassword = function () {
        $state.go('app.forgotpassword');
    };

    $scope.register = function () {
        $state.go('app.register');

    };

    $scope.goPincode = function (pincode) {
       
        sharedUtils.showLoading();
        LoginServices.goPincode(pincode).success(function (data) {
            if (data.status) {
                DashboardService.setItem('items',data.items);
                $state.go('home.dashboard');
            }
            else {
                var alertPopup = $ionicPopup.alert({
                    template: "<center>Something went wrong</center>",
                });
            }
            sharedUtils.hideLoading();
            $scope.user = {};
        }).error(function (data) {
            sharedUtils.hideLoading();
            var alertPopup = $ionicPopup.alert({
                template: "<center>Try Again Later</center>"
            });
        });
    };
}]);