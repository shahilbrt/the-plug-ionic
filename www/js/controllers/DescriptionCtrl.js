app.controller('DescriptionCtrl', ['$scope', '$rootScope', '$state','ModalService', 'sharedUtils', '$ionicModal', '$ionicPopup', '$timeout', '$ionicScrollDelegate','DashboardService', function ($scope, $rootScope, $state, ModalService, sharedUtils, $ionicModal, $ionicPopup, $timeout, $ionicScrollDelegate,DashboardService) {
  $scope.myInterval = 3000;


var product_id= $state.params.id;
console.log(product_id);
 DashboardService.productDescription(product_id).success(function (data) {
     console.log(data);
      if (data.status) {
        $scope.productDes = data.product_details;
        $scope.productImage=data.product_images;
        $scope.otherItems=data.other_items;

      }
      else {
        var alertPopup = $ionicPopup.alert({
          template: "<center>Something went wrong</center>",
        });
      }
      sharedUtils.hideLoading();
    }).error(function (data) {
      sharedUtils.hideLoading();
      var alertPopup = $ionicPopup.alert({
        template: "<center>Try Again Later</center>"
      });
    });


    $scope.description = function (productid) {
        console.log(productid);
        $state.go("home.description",{"id":productid});
    };

  $scope.seller = function () {
    ModalService
      .init('templates/seller.html', $scope)
      .then(function (modal) {
        modal.show();
        $scope.seller = modal;
      });
  };
  
  $scope.sellerData =function (seller_id) {
      $scope.seller();

      DashboardService.getSellerprofile(seller_id).success(function (data) {
          console.log(data);
          if (data.status) {
              $scope.sellerProf=data.seller_profile;
              $scope.sellerProduct=data.seller_products;

          }
          else {
              var alertPopup = $ionicPopup.alert({
                  template: "<center>Something went wrong</center>",
              });
          }
          sharedUtils.hideLoading();
      }).error(function (data) {
          sharedUtils.hideLoading();
          var alertPopup = $ionicPopup.alert({
              template: "<center>Try Again Later</center>"
          });
      });


      
  }
  
  $scope.addFav=function () {
      
  }

  $scope.chat = function () {
    ModalService
      .init('templates/chat.html', $scope)
      .then(function (modal) {
        modal.show();
        $scope.chat = modal;
      });
  };

  $scope.hideTime = true;

  var alternate,
    isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();

  $scope.sendMessage = function () {
    alternate = !alternate;

    var d = new Date();
    d = d.toLocaleTimeString().replace(/:\d+ /, ' ');

    $scope.messages.push({
      userId: alternate ? '12345' : '54321',
      text: $scope.data.message,
      time: d
    });

    delete $scope.data.message;
    $ionicScrollDelegate.scrollBottom(true);

  };


  $scope.inputUp = function () {
    if (isIOS) $scope.data.keyboardHeight = 216;
    $timeout(function () {
      $ionicScrollDelegate.scrollBottom(true);
    }, 300);

  };

  $scope.inputDown = function () {
    if (isIOS) $scope.data.keyboardHeight = 0;
    $ionicScrollDelegate.resize();
  };

  $scope.closeKeyboard = function () {
    // cordova.plugins.Keyboard.close();
  };


  $scope.data = {};
  $scope.myId = '12345';
  $scope.messages = [];


}]);