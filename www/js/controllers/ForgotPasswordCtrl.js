app.controller('ForgotPasswordCtrl', ['$scope', '$state', 'ForgotPasswordServices', '$ionicPopup', 'sharedUtils', function ($scope, $state, ForgotPasswordServices, $ionicPopup, sharedUtils) {
    $scope.user = {};
    $scope.submit = function (data) {
        sharedUtils.showLoading();
        ForgotPasswordServices.forgotpasswordUser(data).success(function (data) {
            sharedUtils.hideLoading();
            if (data.confirmed === 0) {
                $scope.user = {};
                var alertPopup = $ionicPopup.alert({
                    template: "<center>{{ 'EMAILSENDTOYOURMAIL'   }}</center>"
                }).then(function (res) {
                    $state.go('app.login');
                });
            }
            if (data.confirmed === 1) {
                var alertPopup = $ionicPopup.alert({
                    template: "<center>{{ 'NOUSEREXIST'   }}</center>"
                });
            }
        }).error(function (data) {
            sharedUtils.hideLoading();
            var alertPopup = $ionicPopup.alert({
                template: "<center>{{ 'ENTEREDDATAISNOTVALID'   }}</center>"
            });
        });
    };
    $scope.closeForgot = function () {
        $state.go("app.login");
    };
}]);