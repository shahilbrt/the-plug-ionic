// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('app', ['ionic', 'ionic-material', 'ion-datetime-picker', 'chart.js', 'ui.bootstrap', 'rzModule']);

app.run(function ($ionicPlatform, $location, $rootScope, AuthServices, sharedUtils, $ionicPickerI18n, $ionicPopup, $ionicScrollDelegate) {
	$rootScope.popup = false;
	$rootScope.alert = false;
	$ionicPlatform.ready(function () {
		document.addEventListener("offline", internetConn, false);
		if(navigator.splashscreen){
			navigator.splashscreen.hide();
		}
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar) {
			StatusBar.styleDefault();
		}
		document.addEventListener('backbutton', function (e) {
			if (cordova.platformId == 'android') {
				if (window.location.href == 'file:///android_asset/www/index.html#/dashboard' || window.location.href == 'file:///android_asset/www/index.html#/login') {
					navigator.app.exitApp();
				} else {
					navigator.app.backHistory();
				}
			}
		}, false);




        var permissions = cordova.plugins.permissions;

        var list = [
            permissions.CAMERA,
            permissions.GET_ACCOUNTS,
            permissions.ACCESS_FINE_LOCATION,
            permissions.ACCESS_COARSE_LOCATION
        ];

        permissions.checkPermission(list, success, error);

        function error() {
            console.warn('Camera or Accounts permission is not turned on');
        }

        function success( status ) {
            if( !status.hasPermission ) {

                permissions.requestPermissions(
                    list,
                    function(status) {
                        if( !status.hasPermission ) error();
                    },
                    error);
            }
        }

    });
	function internetConn(e) {
		if (e.type == "offline") {
			if (!$rootScope.alert) {
				$rootScope.alert = !$rootScope.alert;
				$ionicPopup.alert({
					title: "Internet Disconnected",
					content: "The internet is disconnected on your device."
				}).then(function (result) {
					if (result) {
						$rootScope.alert = !$rootScope.alert;
						ionic.Platform.exitApp();
					}
				});
			}
		}
	}
});

app.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
	$ionicConfigProvider.backButton.previousTitleText(false);
	$ionicConfigProvider.backButton.text('');
	$ionicConfigProvider.backButton.icon('ion-android-arrow-back');
	$ionicConfigProvider.views.transition("none");
	$ionicConfigProvider.views.swipeBackEnabled(false);
	$ionicConfigProvider.platform.android.scrolling.jsScrolling(false);
	$stateProvider

		.state('app', {
			templateUrl: 'templates/app.html',
			cache: false,
			abstract: true
		})

		.state('app.wizard', {
			url: '/wizard',
			templateUrl: 'templates/wizard.html',
			controller: 'WizardCtrl'
		})

		.state('app.login', {
			url: '/login',
			templateUrl: 'templates/login.html',
			controller: 'LoginCtrl'
		})

		.state('app.forgotpassword', {
			url: '/forgotpassword',
			templateUrl: 'templates/forgotpassword.html',
			controller: 'ForgotPasswordCtrl'
		})

		.state('app.register', {
			url: '/register',
			templateUrl: 'templates/register.html',
			controller: 'RegisterCtrl'
		})

		.state('home', {
			templateUrl: 'templates/sidemenu.html',
			cache: false,
			abstract: true
		})

		.state('home.dashboard', {
			url: '/dashboard',
			cache: false,
			views: {
				'home-tab': {
					templateUrl: 'templates/dashboard.html',
					controller: 'DashboardCtrl'
				}
			}
		})

		.state('home.description', {
			url: '/description/:id',
			cache: false,
			views: {
				'home-tab': {
					templateUrl: 'templates/description.html',
					controller: 'DescriptionCtrl'
				}
			}

		})

		.state('home.profile', {
			url: '/profile',
			views: {
				'profile-tab': {
					templateUrl: 'templates/profile.html',
					controller: 'ProfileCtrl'
				}
			}
		})

			.state('home.uploadproduct', {
			url: '/uploadproduct',
			views: {
				'profile-tab': {
					templateUrl: 'templates/uploadproduct.html',
					controller: 'UploadproductCtrl'
				}
			}
		})

		.state('home.explore', {
			url: '/explore',
			views: {
				'explore-tab': {
					templateUrl: 'templates/explore.html',
					controller: 'ExploreCtrl'
				}
			}
		})

		.state('home.fav', {
			url: '/fav',
			views: {
				'favs-tab': {
					templateUrl: 'templates/fav.html',
					controller: 'FavCtrl'
				}
			}
		})
	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/wizard');
});

app.directive('hideTabs', function ($rootScope) {
	return {
		restrict: 'A',
		link: function ($scope, $el) {
			$rootScope.hideTabs = 'tabs-item-hide';
			$scope.$on('$destroy', function () {
				$rootScope.hideTabs = '';
			});
		}
	};
});


app.constant('CONFIG', (function () {
	  
	return {
		isOffline: false,
		baseUrl:"http://demo.brtindia.com:8080/theplug/public/index.php/api/",
	};
})());


