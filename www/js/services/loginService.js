﻿app.factory('LoginServices',['$q', '$http', 'CONFIG', function ($q, $http, CONFIG) {
    return {
        loginUser: function (EMAIL, PASSWORD) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: "POST",
                url: "http://demo.brtindia.com:8080/theplug/public/index.php/api/auth/login",
                data: {
                    email: EMAIL,
                    password: PASSWORD
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },


         goPincode: function (PINCODE) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: "POST",
                url:CONFIG.baseUrl+"guest_pincode",
                data: {
                    pincode: PINCODE,
                                   
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        }
    };
}]);