﻿app.service('DashboardService', ['$q', '$http', 'CONFIG', function ($q, $http, CONFIG) {
    var items = [];
    return {
        searchProduct: function (reqData) {
            console.log(reqData);
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: aMethod,
                url: CONFIG.baseUrl + "search_product_lists",
                data: {
                    "token": localStorage.getItem("token"),
                    "lat": reqData.latitude,
                    "lng": reqData.longitude,
                    "radius": 20
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },
        productDescription: function (productid) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: aMethod,
                url: CONFIG.baseUrl + "get_product_details",
                data: {
                    "token": localStorage.getItem("token"),
                    "product_id": productid,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },
        getSellerprofile: function (seller_id) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: aMethod,
                url: CONFIG.baseUrl + "get_seller_profile",
                data: {
                    "token": localStorage.getItem("token"),
                    "seller_id": 2,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },



        setItem: function (key, data) {
            items[key] = data;
        },
        getItem: function (key) {
            return items[key];
        }
    };
}]);