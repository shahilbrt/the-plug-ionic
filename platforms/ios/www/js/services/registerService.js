﻿app.factory('RegisterServices',['$q', '$http', 'CONFIG', function ($q, $http, CONFIG) {
    return {
        registerUser: function (DATA) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: aMethod,
                url: "http://demo.brtindia.com:8080/theplug/public/index.php/api/auth/register",
                data: DATA,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        }
    };
}]);