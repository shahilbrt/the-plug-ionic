    app.factory('ProfileServices',['$q', '$http', 'CONFIG', function ($q, $http, CONFIG) {
    return {
        profileUser: function (DATA) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: "GET",
                url:CONFIG.baseUrl+"get_user_profile?token="+localStorage.getItem("token"),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },


        profileUpdate: function (DATA) {

            var data=DATA;
            data.token=localStorage.getItem("token")
            console.log(data);

            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: "POST",
                url:"http://demo.brtindia.com:8080/theplug/public/index.php/api/update_profile",
                data:data,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },

        updatePropic:function(DATA){
                 var deferred = $q.defer();

 return $http({
                method: "POST",
                url:"http://demo.brtindia.com:8080/theplug/public/index.php/api/update_profile_image",
                data:DATA,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });

        }
    };
}]);