﻿app.service('DashboardService',['$q', '$http', 'CONFIG', function ($q, $http, CONFIG) {
    return {
        startCharging: function (data) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: aMethod,
                url: CONFIG.STARTCHARGING,
                data: {"chargeboxid":data.chargeboxid,"outlet":data.outletid},
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("token")
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },
        stopCharging: function (chargebox) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            if(chargebox){
                DATA = {
                    "chargeboxid": chargebox.chargeboxid, "outlet": chargebox.outletid
                };
            }
            else{
                DATA = {};
            }
            return $http({
                method: aMethod,
                url: CONFIG.STOPCHARGING,
                data: DATA,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("token")
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },
        chargingStatus: function () {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            var DATA = {};
            return $http({
                method: aMethod,
                url: CONFIG.GET_CHARGINGSTATUS,
                data: DATA,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("token")
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        }
    };
}]);