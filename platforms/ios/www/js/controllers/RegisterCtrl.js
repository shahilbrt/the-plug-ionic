app.controller('RegisterCtrl', ['$scope', '$state', '$ionicPopup', 'sharedUtils','RegisterServices', function ($scope, $state, $ionicPopup, sharedUtils, RegisterServices) {
    $scope.user = {};
    $scope.registerUser = function (regdata) {
    	sharedUtils.showLoading();
      
                RegisterServices.registerUser(regdata).success(function (data) {
                
                if (data.status) {
                     sharedUtils.hideLoading();
                           
                    localStorage.setItem("token", data.token);
                    localStorage.setItem("refreshtoken", data.refreshtoken);
                    localStorage.setItem("data", JSON.stringify(data));
                    $state.go('home.dashboard');
                }
                else
                {
                     sharedUtils.hideLoading();
                     console.log(data);

                 var alertPopup = $ionicPopup.alert({
                    
                      template: data.errors
                     
                });

                }
            
               
            }).error(function (data) {
                sharedUtils.hideLoading();
                var alertPopup = $ionicPopup.alert({
                    
                    template: "<center>{{ 'TRYAGAINLATER'   }}</center>"
                });
            });
        
        }

        $scope.closeRegister=function(){


            $state.go("app.login");
        }
       
    
}]);