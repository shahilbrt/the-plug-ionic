﻿app.controller('IndexCtrl', ['$scope', '$rootScope', '$state', 'IndexServices', 'sharedUtils', 'ModalService', '$ionicPlatform', '$ionicSideMenuDelegate','ProfileServices', function ($scope, $rootScope, $state, IndexServices, sharedUtils, ModalService, $ionicPlatform, $ionicSideMenuDelegate,ProfileServices) {
	

 ProfileServices.profileUser().success(function (data) {
    $scope.userdata=data.data;

   }).error(function (data) {
    
   });


	$scope.hamburger = true;
	$rootScope.chargingMode = true;
	$scope.default = localStorage.getItem("timeline") ? true : false;
	$scope.image = localStorage.getItem("image") ? localStorage.getItem("image") : './img/camera.png';
	$scope.timeline = localStorage.getItem("timeline") ? localStorage.getItem("timeline") : './img/material-graphite.jpg';
	$scope.userData = JSON.parse(localStorage.getItem("data"));
	$rootScope.fabButton = true;
	$scope.profilePic = function () {
		navigator.camera.getPicture(onSuccess, onFail, {
			quality: 50,
			allowEdit: 1,
			sourceType: 1,
			correctOrientation: 0,
			saveToPhotoAlbum: 1,
			destinationType: Camera.DestinationType.FILE_URI
		});
		function onSuccess(imageURI) {
			$scope.image = imageURI;
			localStorage.setItem("image", imageURI);
			$scope.$digest();
		}
		function onFail(message) {
		}
	};

	$scope.backgroundPic = function () {
		navigator.camera.getPicture(onSuccess, onFail, {
			quality: 50,
			destinationType: Camera.DestinationType.DATA_URL,
			sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
		});
		function onSuccess(imageURI) {
			$scope.default = true;
			$scope.timeline = imageURI;
			localStorage.setItem("timeline", imageURI);
			$scope.$digest();
		}
		function onFail(message) {
		}
	};

	$scope.logout = function () {
		sharedUtils.showLoading();
		IndexServices.logout().success(function (data) {
			localStorage.clear();
			$state.go("app.login");
			sharedUtils.hideLoading();
		}).error(function (data) {
			localStorage.clear();
			$state.go("app.login");
			sharedUtils.hideLoading();
		});
	};

	$scope.invite = function () {
		ModalService
			.init('templates/invite.html', $scope)
			.then(function (modal) {
				modal.show();
			});
	};

	$scope.complain = function () {
		ModalService
			.init('templates/complain.html', $scope)
			.then(function (modal) {
				modal.show();
				$scope.complain = modal;
			});
	};

	$scope.orders = function () {
		ModalService
			.init('templates/orders.html', $scope)
			.then(function (modal) {
				modal.show();
			});
	};

	$scope.terms = function () {
		ModalService
			.init('templates/terms.html', $scope)
			.then(function (modal) {
				modal.show();
			});
	};

	$scope.complainMail = function () {
		ModalService
			.init('templates/complainmail.html', $scope)
			.then(function (modal) {
				modal.show();
				$scope.complainmail = modal;
			});
	};

	$scope.profilePage = function () {
		$state.go("home.profile");
	};

	$scope.explore = function () {
		$state.go("home.explore");
	};

	$scope.fav = function () {
		$state.go("home.fav");
	};

	$scope.dashboard = function () {
		$state.go("home.dashboard");
	};

	$scope.$watch(function () {
		return $ionicSideMenuDelegate.isOpenLeft();
	},
		function (isOpen) {
			if (isOpen) {
				$rootScope.fabButton = false;
			}
			else {
				$rootScope.fabButton = true;
			}
		});
}]);