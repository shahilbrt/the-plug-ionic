﻿app.controller('DashboardCtrl', ['$scope', '$rootScope', '$state', 'DashboardService', 'sharedUtils', '$ionicModal', '$ionicPopup', function ($scope, $rootScope, $state, DashboardService, sharedUtils, $ionicModal, $ionicPopup) {
	$scope.menu = {
    "item1" : {
      "handler" : "cat01",
      "image" : "chicken_maharaja",
      "name" : "New Chicken",
      "distance": 0.3,
      "price" : 130,
      "stock" : 100
    },
    "item2" : {
      "handler" : "cat01",
      "image" : "big_spicy_chicken_wrap",
      "name" : "Big Spicy",
      "distance": 0.5,
      "price" : 120,
      "stock" : 100
    }};
    $scope.description = function () {
		  $state.go("home.description");
	  };
}]);