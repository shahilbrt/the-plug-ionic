﻿app.controller('LoginCtrl',['$scope', 'LoginServices', '$ionicPopup', '$state', 'sharedUtils',function ($scope, LoginServices, $ionicPopup, $state, sharedUtils) {
    $scope.user = {};
    $scope.login = function (formName, cred) {
        if (formName.$valid) {
            sharedUtils.showLoading();
            LoginServices.loginUser(cred.email, cred.password).success(function (data) {
                
                if (data.status) {
                    sharedUtils.hideLoading();
                    localStorage.setItem("token", data.token);
                    localStorage.setItem("refreshtoken", data.refreshtoken);
                    localStorage.setItem("data", JSON.stringify(data));
                    $state.go('home.dashboard');
                }
                else
                {
                 sharedUtils.hideLoading();
                    var alertPopup = $ionicPopup.alert({
                        template: "<center>{{' Invalid Username & Password'}}</center>",

                    });

                }
                /*else if (data.confirmed === 1) {
                    var alertPopup = $ionicPopup.alert({
                        template: "<center>{{ 'UNABLETOLOGIN'   }}</center>"
                    });
                }
                else if (data.confirmed === 2) {
                    var alertPopup = $ionicPopup.alert({
                         template: "<center>{{ 'NOUSEREXIST'   }}</center>"
                    });
                }
                else {
                    var alertPopup = $ionicPopup.alert({
                        template: "<center>{{ 'RECHECKCREDENTIALS'   }}</center>"

                    });
                }*/
                sharedUtils.hideLoading();
                $scope.user = {};
            }).error(function (data) {
                sharedUtils.hideLoading();
                var alertPopup = $ionicPopup.alert({
                    
                    template: "<center>{{ 'TRYAGAINLATER'   }}</center>"
                });
            });
        
        }
        else {
        }
    };

    $scope.forgotPassword = function () {
        $state.go('app.forgotpassword');
    };
    $scope.register = function () {
        $state.go('app.register');

    };

    $scope.goPincode= function (pincode){
        
        
        console.log(pincode);

         LoginServices.goPincode(pincode).success(function (data) {
                
                if (data.status) {
                    sharedUtils.hideLoading();
                    
                    $state.go('home.dashboard');
                }
                else
                {
                 sharedUtils.hideLoading();
                   

                }
             
                sharedUtils.hideLoading();
                $scope.user = {};
            }).error(function (data) {
                sharedUtils.hideLoading();
                var alertPopup = $ionicPopup.alert({
                    
                    template: "<center>{{ 'TRYAGAINLATER'   }}</center>"
                });
            });
        


    };
}]);