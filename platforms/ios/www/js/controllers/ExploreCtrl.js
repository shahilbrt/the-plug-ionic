app.controller('ExploreCtrl', ['$scope', '$rootScope', '$state', 'DashboardService', 'sharedUtils', '$ionicModal', '$ionicPopup', function ($scope, $rootScope, $state, DashboardService, sharedUtils, $ionicModal, $ionicPopup) {
	function initialize() {

	var myLatlng = new google.maps.LatLng(-31.9546781,115.852662);
	var mapOptions = {
		zoom: 14,
		center: myLatlng,
		disableDefaultUI: true
	}
	
	var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	/*
	// Example standard marker
	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		title: 'Hello World!'
	});
	*/
	
	overlay = new CustomMarker(
		myLatlng, 
		map,
		{
			marker_id: '123'
		}
	);
}

google.maps.event.addDomListener(window, 'load', initialize);

function CustomMarker(latlng, map, args) {
	this.latlng = latlng;	
	this.args = args;	
	this.setMap(map);	
}

CustomMarker.prototype = new google.maps.OverlayView();

CustomMarker.prototype.draw = function() {
	
	var self = this;
	
	var div = this.div;
	
	if (!div) {
	
		div = this.div = document.createElement('div');
		
		div.className = 'marker';
		
		div.style.position = 'absolute';
		div.style.cursor = 'pointer';
		div.style.width = '20px';
		div.style.height = '20px';
		div.style.background = 'blue';
		
		if (typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}
		
		google.maps.event.addDomListener(div, "click", function(event) {			
			google.maps.event.trigger(self, "click");
		});
		
		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}
	
	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);
	
	if (point) {
		div.style.left = point.x + 'px';
		div.style.top = point.y + 'px';
	}
};

CustomMarker.prototype.remove = function() {
	if (this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}	
};

CustomMarker.prototype.getPosition = function() {
	return this.latlng;	
};
initialize()
}]);