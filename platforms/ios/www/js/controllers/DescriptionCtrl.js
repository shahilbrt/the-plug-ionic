app.controller('DescriptionCtrl', ['$scope', '$rootScope', '$state', 'ModalService', 'sharedUtils', '$ionicModal', '$ionicPopup','$timeout', '$ionicScrollDelegate', function ($scope, $rootScope, $state, ModalService, sharedUtils, $ionicModal, $ionicPopup, $timeout, $ionicScrollDelegate) {
	 $scope.myInterval = 3000;
  $scope.slides = [
  
    {
      image: 'http://lorempixel.com/400/200/people'
    }
  ];

  $scope.seller = function () {
		ModalService
			.init('templates/seller.html', $scope)
			.then(function (modal) {
				modal.show();
				$scope.seller = modal;
			});
	};

	$scope.chat = function () {
		ModalService
			.init('templates/chat.html', $scope)
			.then(function (modal) {
				modal.show();
				$scope.chat = modal;
			});
	};

	$scope.hideTime = true;

  var alternate,
    isIOS = ionic.Platform.isWebView() && ionic.Platform.isIOS();

  $scope.sendMessage = function() {
    alternate = !alternate;

    var d = new Date();
  d = d.toLocaleTimeString().replace(/:\d+ /, ' ');

    $scope.messages.push({
      userId: alternate ? '12345' : '54321',
      text: $scope.data.message,
      time: d
    });

    delete $scope.data.message;
    $ionicScrollDelegate.scrollBottom(true);

  };


  $scope.inputUp = function() {
    if (isIOS) $scope.data.keyboardHeight = 216;
    $timeout(function() {
      $ionicScrollDelegate.scrollBottom(true);
    }, 300);

  };

  $scope.inputDown = function() {
    if (isIOS) $scope.data.keyboardHeight = 0;
    $ionicScrollDelegate.resize();
  };

  $scope.closeKeyboard = function() {
    // cordova.plugins.Keyboard.close();
  };


  $scope.data = {};
  $scope.myId = '12345';
  $scope.messages = [];


}]);