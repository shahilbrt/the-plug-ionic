app.factory('IndexServices',['$q', '$http', 'CONFIG', function ($q, $http, CONFIG) {
    return {
        logout : function () {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: 'POST',
                url: CONFIG.Logout,

                data: {

                },
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("token")
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },
        getorders : function () {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: 'POST',
                url: CONFIG.baseUrl+'get_orders',
                data: {
                    "token":localStorage.getItem("token"),
                    "range":""

                },
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("token")
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        },
        getcomplaint : function (data) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: 'POST',
                url: CONFIG.baseUrl+'complaint_form',
                data: {
                    "token":localStorage.getItem("token"),
                    "order_id":data

                },
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("token")
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        }
    };
}]);