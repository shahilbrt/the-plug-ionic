app.factory('ForgotPasswordServices',['$q', '$http', 'CONFIG', function ($q, $http, CONFIG) {
    return {
        forgotpasswordUser: function (email) {
            var deferred = $q.defer();
            var aMethod = CONFIG.isOffline ? "GET" : "POST";
            return $http({
                method: aMethod,
                url: CONFIG.FORGOTPASSWORD,
                data: {
                    "email":email
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        }
    };
}]);