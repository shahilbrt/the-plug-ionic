app.factory('AuthServices',['$q', '$http', 'CONFIG', function ($q, $http, CONFIG) {
    return {
        auth: function (refreshToken) {
            var deferred = $q.defer();
            var aMethod = "POST";
            if(refreshToken){
                DATA = {
                    "refreshtoken": refreshToken
                };
            }
            else{
                DATA = {};
            }
            return $http({
                method: aMethod,
                url: CONFIG.AUTH,
                data: DATA,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem("token")
                }
            }).success(function (response) {
                deferred.resolve(response);
            }).error(function (response, status) {
                deferred.reject(response);
            });
        }
    };
}]);