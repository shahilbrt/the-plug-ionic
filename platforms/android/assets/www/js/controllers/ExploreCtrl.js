app.controller('ExploreCtrl', function ($scope, $ionicLoading, $compile,DashboardService, $ionicPopup) {

	$scope.menu=[];

    navigator.geolocation.getCurrentPosition(function (pos) {
        $scope.location(pos.coords);

        $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
    }, function (error) {
        alert('Unable to get location: ' + error.message);
    });

	$scope.location = function (reqData) {

	DashboardService.searchProduct(reqData).success(function (data) {
      if (data.status) {
        $scope.menu = data.items;
        console.log($scope.menu);
        google.maps.event.addDomListener(window, 'load', $scope.init());

      }
      else {
        var alertPopup = $ionicPopup.alert({
          template: "<center>Something went wrong</center>",
        });
      }
    }).error(function (data) {
      var alertPopup = $ionicPopup.alert({
        template: "<center>Try Again Later</center>"
      });
    });

    }
	$scope.init = function () {
		console.log('inside map');
		var myLatlng = new google.maps.LatLng(12.9623574, 77.5929584);

      		var mapOptions = {
			center: myLatlng,
			zoom: 13,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(document.getElementById("map"),
			mapOptions);

		//Marker + infowindow + angularjs compiled ng-click
		var contentString = "<div ng-click='clickTest()'>Click me!</div>";
		var compiled = $compile(contentString)($scope);

		var infowindow = new google.maps.InfoWindow({
			content: compiled[0]
		});

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title: 'Uluru (Ayers Rock)'
		});

	
		$scope.map = map;

		// Additional Markers //
        $scope.markers = [];
        var infoWindow = new google.maps.InfoWindow();
        var createMarker = function (info){
        	console.log(info);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(info.lat, info.lng),
                map: $scope.map,
                animation: google.maps.Animation.DROP,
                title: info.product_title
            });
            marker.content = '<div class="infoWindowContent">' + info.desc + '</div>';
            google.maps.event.addListener(marker, 'click', function(){
                infoWindow.setContent('<h2>' + marker.product_title + '</h2>' + marker.content);
                infoWindow.open($scope.map, marker);
            });
            $scope.markers.push(marker);
        }  
        for (i = 0; i <  $scope.menu.length; i++){
            createMarker( $scope.menu[i]);
        }

	}

        
	$scope.clickTest = function () {
		alert('Example of infowindow with ng-click')
	};
   $scope.init();

});