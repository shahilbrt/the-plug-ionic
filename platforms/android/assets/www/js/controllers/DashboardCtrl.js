﻿app.controller('DashboardCtrl', ['$scope', '$rootScope', '$state', 'DashboardService', 'sharedUtils', '$ionicModal', '$ionicPopup', function ($scope, $rootScope, $state, DashboardService, sharedUtils, $ionicModal, $ionicPopup) {



    navigator.geolocation.getCurrentPosition(function (pos) {
        $scope.loc(pos.coords);

        return pos.coords;
    }, function (error) {
      var alertPopup = $ionicPopup.alert({
        template: "<center>Unable to find location</center>"
      });
    });


    $scope.loc = function (data) {
      console.log(data);

        if (localStorage.getItem("token")) {

            sharedUtils.showLoading();
            DashboardService.searchProduct(data).success(function (data) {
                if (data.status) {
                    console.log(data);
                    $scope.menu = data.items;
                }
                else {
                    var alertPopup = $ionicPopup.alert({
                        template: "<center>Something went wrong</center>",
                    });
                }
                sharedUtils.hideLoading();
            }).error(function (data) {
                sharedUtils.hideLoading();
                var alertPopup = $ionicPopup.alert({
                    template: "<center>Try Again Later</center>"
                });
            });
        } else {
            $scope.menu = DashboardService.getItem('items');
        }


  };

  $scope.filter = false;
  $scope.menu = {
    "item1": {
      "handler": "cat01",
      "image": "chicken_maharaja",
      "name": "New Chicken",
      "distance": 0.3,
      "price": 130,
      "stock": 100
    },
    "item2": {
      "handler": "cat01",
      "image": "big_spicy_chicken_wrap",
      "name": "Big Spicy",
      "distance": 0.5,
      "price": 120,
      "stock": 100
    }
  };
  $scope.description = function (productid) {
    console.log(productid);
    $state.go("home.description",{"id":productid});
  };
  $scope.filterMenu = function () {
    $scope.filter = !$scope.filter;
  };
  $scope.slider = {
    minValue: 10,
    maxValue: 90,
    options: {
      floor: 0,
      ceil: 100,
      translate: function (value) {
        return value + " mi";
      },
      step: 10,
      showTicks: true
    }
  };

  
}]);