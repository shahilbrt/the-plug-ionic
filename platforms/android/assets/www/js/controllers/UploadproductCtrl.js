app.controller('UploadproductCtrl', ['$scope', '$state', '$ionicSlideBoxDelegate','$ionicActionSheet','$timeout', function ($scope, $state, $ionicSlideBoxDelegate,$ionicActionSheet,$timeout) {

    $scope.uploadProduct=function (upload) {
        console.log(upload);
        $scope.uploadData=upload;

    }

    $scope.openActionsheet = function() {

        // Show the action sheet
        var showActionSheet = $ionicActionSheet.show({
            buttons: [
                { text: 'Capture' },
                { text: 'Select Image' }
            ],

            destructiveText: 'Delete',
            titleText: 'Action Sheet',
            cancelText: 'Cancel',

            cancel: function() {
                // add cancel code...
            },

            buttonClicked: function(index) {
                if(index === 0) {
                    // add edit 1 code
                    $scope.takeImage();
                }

                if(index === 1) {
                    // add edit 2 code
                    $scope.selectImage();
                }
            },

            destructiveButtonClicked: function() {
                // add delete code..
            }
        });
    };

    $scope.takeImage = function() {
        navigator.camera.getPicture(onSuccess, onFail, {
            quality: 50,
            allowEdit: 1,
            sourceType: 1,
            correctOrientation: 0,
            encodingType: Camera.EncodingType.JPEG,
            saveToPhotoAlbum: 1,
            destinationType: Camera.DestinationType.DATA_URL

        });
        function onSuccess(imageURI) {

            var image = document.getElementById('myImage');
            $scope.image = "data:image/jpeg;base64," + imageURI;
            console.log($scope.image);

            localStorage.setItem("image", imageURI);
            $scope.uploadPicture();
            $scope.$digest();

        }
        function onFail(message) {
        }
    }

    $scope.selectImage = function() {
        console.log("close")
        $scope.srcImage = [];
        window.imagePicker.getPictures(
            function(results) {

                for (var i = 0; i < results.length; i++) {
                    $scope.imageUri = results[i];
                    window.plugins.Base64.encodeFile($scope.imageUri, function(base64) {
                        $scope.srcImg = base64;
                        $scope.srcImage.push({
                            "image": base64
                        });
                    });
                }
                $timeout(function() {

                    $scope.srcImagedata = $scope.srcImage;
                    console.log($scope.srcImagedata.length);
                    if ($scope.srcImagedata.length != 0) {

                    }

                }, 200);
            },
            function(error) {
                console.log('Error: ' + error);
            },

            {

                maximumImagesCount: 10,
                width: 800
            }
        );

    }




    $scope.uploadPicture = function() {
        console.log($scope.srcImagedata);
        var win = function(r) {
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
        }

        var fail = function(error) {
            alert("An error has occurred: Code = " + error.code);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
        }
        $ionicLoading.show({
            template: 'Image Uploading....'
        });


        var fileURL = $scope.srcImg;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";

        options.chunkedMode = true;

        var params = {};
        params.user_id = $scope.loginDtls.id;
        params.matrimony_id = $scope.loginDtls.matrimony_id;
        params.religion = $scope.loginDtls.religion;
        params.mother_tongue = $scope.loginDtls.mother_tongue;


        options.params = params;


        var ft = new FileTransfer();

        ft.upload(fileURL, encodeURI(""), win, fail, options);

        console.log(win);
        console.log(fail);
        console.log(options);

    }

}]);