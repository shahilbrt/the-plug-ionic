﻿app.controller('ProfileCtrl', function ($scope, ProfileServices, $http) {
  ProfileServices.profileUser().success(function (data) {
    $scope.userdata = data.data;

  }).error(function (data) {

  });
  $scope.edit = true;
  $scope.user = "";
  $scope.editProfile = function () {
    $scope.edit = !$scope.edit;
  };

  $scope.save = function (data) {
    $scope.edit = !$scope.edit;


    ProfileServices.profileUpdate(data).success(function (data) {

      $scope.userdata = data.data;

    }).error(function (data) {

    });


  };

  $scope.cancel = function () {
    $scope.edit = !$scope.edit;
    $scope.user = "";
  };

  $scope.profilePic = function () {
    navigator.camera.getPicture(onSuccess, onFail, {
      quality: 50,
      allowEdit: 1,
      sourceType: 1,
      correctOrientation: 0,
      encodingType: Camera.EncodingType.JPEG,
      saveToPhotoAlbum: 1,
      destinationType: Camera.DestinationType.DATA_URL

    });
    function onSuccess(imageURI) {

      var image = document.getElementById('myImage');
      $scope.image = "data:image/jpeg;base64," + imageURI;
      console.log($scope.image);

      localStorage.setItem("image", imageURI);
      $scope.uploadPic();
      $scope.$digest();

    }
    function onFail(message) {
    }
  };

  $scope.uploadPropic = function () {

    var params = {};

    params.profile_image = $scope.image;
    params.token = localStorage.getItem("token");

    ProfileServices.updatePropic(params).success(function (data) {
      $scope.userdata.profile_image = data.data;
    }).error(function (data) {

    });

  }
});