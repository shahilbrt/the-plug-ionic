var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var concatCss = require('gulp-concat-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var htmlmin = require('gulp-htmlmin');
var htmlreplace = require('gulp-html-replace');
var imagemin = require('gulp-imagemin');
var ngAnnotate = require('gulp-ng-annotate');

gulp.task('default', ['minify-css','compress','replace','minify','minifyImg','fonts']);

gulp.task('minify-css',() => {
  return gulp.src([
  './www/lib/ionic/release/css/ionic.min.css',
  './www/lib/ionic-material/dist/ionic.material.min.css',
  './www/lib/ion-datetime-picker/release/ion-datetime-picker.min.css',
  './www/css/style.css'
  ])
    .pipe(sourcemaps.init())
    .pipe(concatCss('style.css'))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/www/css/'));
});

gulp.task('compress', function () {
  return gulp.src([
  './www/lib/ionic/release/js/ionic.bundle.js',
  './www/lib/chart.js/dist/Chart.min.js',
  './www/lib/angular-chart.js/dist/angular-chart.min.js',
  './www/lib/ionic-material/dist/ionic.material.min.js',
  './www/lib/ion-datetime-picker/release/ion-datetime-picker.min.js',
  './www/lib/angular-translate/angular-translate.js',
  './www/js/*.js',
  './www/js/services/commonServices/*.js',
  './www/js/services/*.js',
  './www/js/directives/*.js',
  './www/js/controllers/*.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/www/js/'));
});

gulp.task('replace', function() {
  gulp.src('./www/index.html')
    .pipe(htmlreplace({
        'css': 'css/style.css',
        'js': ['js/app.js','cordova.js']
    }))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist/www/'));
});

gulp.task('minify', function() {
  return gulp.src('./www/templates/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('./dist/www/templates/'));
});

gulp.task('minifyImg', () =>
    gulp.src('./www/img/*')
        .pipe(imagemin({ progressive: true }))
        .pipe(gulp.dest('dist/www/img'))
);

gulp.task('fonts', () =>
    gulp.src('./www/lib/ionic/release/fonts/*')
        .pipe(gulp.dest('dist/www/fonts'))
);

